import os
import sys
import ffmpy
import ffmpeg
import subprocess


def get_frame_rate(filename):
    if not os.path.exists(filename):
        sys.stderr.write("ERROR: filename %r was not found!" % (filename,))
        return -1
    out = subprocess.check_output(["ffprobe",filename,"-v","0","-select_streams","V:0","-print_format","flat",
                                   "-show_entries","stream=r_frame_rate"])
    strout = str(out)
    proper_rate = strout.split('"')[1].split("/")
    if len(proper_rate)==1:
        return float(proper_rate[0])
    if len(proper_rate)==2:
        return float(proper_rate[0])/float(proper_rate[1])
    return -1


if __name__ == '__main__':
    video = os.path.join("E:\\NightTimeVideo\\Videos", "night_time_video.mp4")
    # fps = subprocess.Popen("ffprobe -v 0 -of csv=p=0 -select_streams V:0 -show_entries stream=r_frame_rate {}".format(video))
    fps = get_frame_rate(video)
    print("Test :: {}".format(fps))