import os
import sys
import cv2
import argparse
from datetime import datetime, timedelta
import traceback


class VideoData:

    video_formats = ["mp4", "avi", "mts", "mkv", "ts", "flv",
                      "webm", "flv", "mov", "ogg", "wmv", "mpg",
                      "mpeg", "3gp", "svi", "mxf", "roq", "nsv"]

    def __init__(self):
        self.start_time = None
        self.frame_rate = None
        self.time_zone = None
        self.time_lapse = None
        self.video_location = None
        self.frame_count = None
        self.time_print_format = None
        self.strf_24 = '%Y-%m-%d %H:%M:%S'
        self.strf_12 = '%Y-%m-%d %I:%M:%S'
        self.strf_frame = '%H:%M:%S,%f'
        self.intertime_sign = '-->'
        self.video_name = None
        self.video_folder = None
        self.iterator_time = None

    def set_values(self, start_time, frame_rate, time_zone, time_lapse, video_location, frame_count, print_format,
                   video_name, video_folder):
        self.start_time = start_time
        self.iterator_time = start_time
        self.frame_rate = frame_rate
        self.time_lapse = time_lapse
        self.time_zone = time_zone
        self.video_location = video_location
        self.frame_count = frame_count
        self.time_print_format = print_format
        self.video_folder = video_folder
        self.video_name = video_name


def create_subtitles_file(vi):
    time_per_frame = 1/vi.frame_rate
    try:
        with open(vi.video_folder + vi.video_name + ".srt", 'w+') as subtitles_file:
            print(vi.frame_count)
            for _frame in range(int(vi.frame_count)):
                frame = _frame + 1
                this_frame_start_time = datetime.utcfromtimestamp(frame*time_per_frame).strftime(vi.strf_frame)
                this_frame_start_time = this_frame_start_time[:-3]
                this_frame_end_time = datetime.utcfromtimestamp((frame+1)*time_per_frame).strftime(vi.strf_frame)
                this_frame_end_time = this_frame_end_time[:-3]
                if vi.time_lapse is not None:
                    real_time = vi.start_time + timedelta(seconds=(vi.time_lapse * _frame))
                else:
                    real_time = vi.start_time + timedelta(seconds=time_per_frame * _frame)

                if vi.time_print_format == 12:
                    this_frame_real_time = real_time.strftime(vi.strf_12)
                elif vi.time_print_format == 24:
                    this_frame_real_time = real_time.strftime(vi.strf_24)
                else:
                    break
                subtitles_file.write("{frame}\n{st} {sign} {et}\n{rt}\n\n".format(frame=frame,
                                                                               st=this_frame_start_time,
                                                                               sign=vi.intertime_sign,
                                                                               et=this_frame_end_time,
                                                                               rt=this_frame_real_time))

    except Exception as exc:
        traceback.print_exc()
        print(exc)
        print("eeh~....")
    return True


def is_a_valid_video_path(video_loc):
    if not os.path.isfile(video_loc):
        return False, False, False
    if "\\" in video_loc:
        extension = video_loc.split("\\")[-1].split(".")[-1]
        name = video_loc.split("\\")[-1].split(".")[0]
        path = video_loc.split(name)[0]
    elif "/" in video_loc:
        extension = video_loc.split("/")[-1].split(".")[-1]
        name = video_loc.split("/")[-1].split(".")[0]
        path = video_loc.split(name)[0]
    else:
        return False, False, False
    if str.lower(extension) in VideoData.video_formats:
        return True, name, path
    return False, False, False


def get_creation_time(file_location, s_t, t_z, f_c, f_r, t_l):
    cur_platform = sys.platform
    if s_t is not None:
        try:
            time_zone = process_time_zone(t_z)
            s_t = datetime.strptime(s_t, "%Y-%m-%d %H:%M:%S").utctimetuple() + time_zone
            if t_l is not None:
                s_t = s_t - timedelta(seconds=f_c * t_l)
            else:
                s_t = s_t - timedelta(seconds=f_c/f_r)
            return s_t
        except Exception as exc:
            traceback.print_exc()
            pass

    if "linux" in cur_platform:
        try:
            s_t = os.stat(file_location).st_ctime
            return s_t
        except:
            return None
    if "win" in cur_platform:
        try:
            time_zone = process_time_zone(t_z)
            ctime = os.path.getctime(file_location)
            mtime = os.path.getmtime(file_location)
            if ctime>mtime:
                start_time = datetime.utcfromtimestamp(mtime) + time_zone
            else:
                start_time = datetime.utcfromtimestamp(ctime) + time_zone
            if t_l is not None:
                start_time = start_time - timedelta(seconds=f_c * t_l)
            else:
                start_time = start_time - timedelta(seconds=f_c / f_r)
            start_time = datetime.timestamp(start_time)
            return start_time
        except OSError as osexc:
            print(osexc)
            traceback.print_exc()
        except Exception as exc:
            traceback.print_exc()
    if "mac" in cur_platform or "darwin" in cur_platform:
        start_time = os.stat(file_location).st_birthtime
        return start_time


def process_time_zone(timecodeformat):
    hours = int(timecodeformat)
    minutes = (timecodeformat - hours) * 60
    this_time_delta = timedelta(hours=hours, minutes=minutes)
    return this_time_delta


def establish_data_validity(v_loc, creation_time, framerate, timecodeformat, t_z, t_l):
    valid, name, path = is_a_valid_video_path(v_loc)
    if not valid:
        return None, "Video Path Is Not Valid"

    video = cv2.VideoCapture(v_loc)
    if framerate is None:
        try:
            framerate = video.get(cv2.CAP_PROP_FPS)
        except:
            return None, "Video framerate cannot be extracted, enter it manually."

    total_frames = video.get(cv2.CAP_PROP_FRAME_COUNT)

    creation_time = get_creation_time(v_loc, creation_time, t_z, total_frames, framerate, t_l)
    if creation_time is None:
        return None, "Could not get Creation Time, Input it manually"


    this_video_time_delta = process_time_zone(t_z)

    this_datetime = datetime.utcfromtimestamp(creation_time)


    video_information = VideoData()
    video_information.set_values(this_datetime + this_video_time_delta, framerate, this_video_time_delta, t_l, v_loc,
                                 total_frames, timecodeformat, name, path)
    return video_information, "OK"


def input_checker(_input, variable):
    try:
        thisinput = float(_input)
    except:
        while True:
            try:
                if variable == 1:
                    userinput = input("Enter the value of time zone as follows \n"
                                      "5.5 for PKT. 0 for GMT. Hours are integers \n"
                                      "and then each fifteen minutes is equal to .25.\n"
                                      "::")
                else:
                    userinput = input("Enter the timelapse seconds setting.\n"
                                      "The seconds between each frame.\n"
                                      "::")
                thisinput = float(userinput)
                break
            except:
                pass
    finally:
        return thisinput


def process(_video_location, _start_time, _fps, _formattype, _timezone, _timelapse):
    info, status = establish_data_validity(_video_location, _start_time, _fps, _formattype, _timezone, _timelapse)
    if info is None:
        print(status)
    create_subtitles_file(info)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--video", default=None, dest="video",
                        help="The location of the video.")
    parser.add_argument("-s", "--starttime", default=None, dest="starttime",
                        help="The start time of the video. If the video meta-data"
                             "does not hold this information, then, this is necessary."
                             " On Linux platforms, this is necessary."
                             "Format type = Year-month-day Hour:Minute:Second, 24 hours version")
    parser.add_argument("-f", "--fps", default=None, dest="fps",
                        help="The frames per second of the video. If meta-data is inaccessible,"
                             "This argument wil be deemed necessary.")
    parser.add_argument("-m", "--format", default=24, dest="format",
                        help="The Time code format required. 12hr, 24hr. An integer with an enumerator of 12 and 24")
    parser.add_argument("-t", "--timezone", default=5, dest="timezone",
                        help="Enter the time zone of the video.")
    parser.add_argument("-l", "--timelapse", default=None, dest="timelapse",
                        help="The time between each frame from the timelapse setting. In seconds.")

    arguments = parser.parse_args()
    _video_location = arguments.video
    _start_time = arguments.starttime
    _fps = arguments.fps
    _formattype = arguments.format
    _timezone = input_checker(arguments.timezone, 1)
    _timelapse = arguments.timelapse
    if _timelapse is not None:
        _timelapse = input_checker(_timelapse, 2)

    process(_video_location, _start_time, _fps, _formattype, _timezone, _timelapse)
