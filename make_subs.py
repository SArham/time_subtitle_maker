import os
import argparse
from glob import glob
import json
from datetime import datetime, timedelta
import traceback
import subtitle_maker as sm
from gooey import Gooey, GooeyParser



"""json formatting:
    video name will be just the name of the video with the extension. The path shoud not be included.
    start_time = "Year-month-day Hour:Minute:Second, 24 hours version" 2018-11-24 17:23:21
    fps = integer i.e. numbers
    formattype = 12 or 24
    timezone = Pakistan is 5, India is 5.5
    timelapse = Leave as None if not a timelapse video, otherwise, write the seconds between each frame.
    {
        "video_name_1":[start_time, fps, formattype, timezone, timelapse],
        "video_name_2":[start_time, fps, formattype, timezone, timelapse],
        .
        .
        .
    }
    """


def example_time_input():
    s_t = "2018-11-24 17:23:21"
    o_s_t = datetime.strptime(s_t, "%Y-%m-%d %H:%M:%S") + timedelta(hours=5, seconds=30)
    print("{}  {}  {}".format(s_t, "-->", o_s_t))


def process_json(json_loc):
    return json.load(open(json_loc, 'r'))


def differentiate_videos(video_name):
    pass


@Gooey(advanced=bool,
       program_name="Subtitle Maker")
def main():
    parser = GooeyParser()
    parser.add_argument("-f", "--folder", dest="folder", default=None,
                        help="Location of folder with videos", widget="DirChooser")
    parser.add_argument("-j", "--json", dest="json", default=None,
                        help="The location of the json with video information", widget="FileChooser")
    parser.add_argument("-t", "--timelapse", dest="timelapse", default=False, type=bool,
                        help="Are all the videos in the folder Time Lapse videos.")

    arguments = parser.parse_args()
    _folder = arguments.folder
    _json = arguments.json
    _timelapse = arguments.timelapse
    if not os.path.isdir(_folder):
        exit("Location given is not a valid folder path")
    video_formats = ["mp4", "avi", "mts", "mkv", "ts", "flv",
                     "webm", "flv", "mov", "ogg", "wmv", "mpg",
                     "mpeg", "3gp", "svi", "mxf", "roq", "nsv",
                     "mod"]

    files = []
    for ext in video_formats:
        c_file = glob(os.path.join(_folder, "*." + ext))
        for file in c_file:
            files.append(file)

    if len(files) == 0:
        exit("Folder has no valid video files in it")
    if _timelapse:
        timelapse = 0.5
    else:
        timelapse = None
    if _json is not None:
        video_data = process_json(_json)
        if len(video_data) > 0:
            for file in files:
                try:
                    start_time, fps, formattype, timezone, timelapse = video_data[file]
                    sm.process(file, start_time, fps, formattype, timezone, timelapse)
                except Exception as exc:
                    try:
                        start_time = None
                        fps = None
                        formattype = 24
                        timezone = 5
                        sm.process(file, start_time, fps, formattype, timezone, timelapse)
                    except Exception as exc:
                        print(file)
                        print(exc)
                        traceback.print_exc()
    else:
        for file in files:
            try:
                start_time = None
                fps = None
                formattype = 24
                timezone = 5
                sm.process(file, start_time, fps, formattype, timezone, timelapse)
            except Exception as exc:
                print(file)
                print(exc)
                traceback.print_exc()

if __name__ == '__main__':
    main()
